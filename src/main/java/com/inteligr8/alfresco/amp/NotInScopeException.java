/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.amp;

import org.eclipse.aether.artifact.Artifact;

/**
 * An exception delineating when an artifact is not-in-scope.
 * 
 * @author brian@inteligr8.com
 */
public class NotInScopeException extends Exception {

	private static final long serialVersionUID = 8055701654626788700L;
	
	private final Artifact notInScopeParentArtifact;
	private Artifact impactedChildArtifact;
	
	/**
	 * @param notInScopeParentArtifact A Maven artifact not considered in-scope.
	 */
	public NotInScopeException(Artifact notInScopeParentArtifact) {
		super("The '" + notInScopeParentArtifact.getArtifactId() + "' artifact is not scope");
		this.notInScopeParentArtifact = notInScopeParentArtifact;
	}
	
	/**
	 * @param notInScopeParentArtifact A Maven artifact not considered in-scope.
	 * @param impactedChildArtifact A Maven artifact considered to be in-scope.
	 */
	public NotInScopeException(Artifact notInScopeParentArtifact, Artifact impactedChildArtifact) {
		super("The '" + notInScopeParentArtifact.getArtifactId() + "' artifact is not scope, impacting the '" + impactedChildArtifact.getArtifactId() + "' artifact");
		this.notInScopeParentArtifact = notInScopeParentArtifact;
		this.impactedChildArtifact = impactedChildArtifact;
	}
	
	/**
	 * @return A Maven artifact not considered in-scope.
	 */
	public Artifact getNotInScopeParentArtifact() {
		return this.notInScopeParentArtifact;
	}
	
	/**
	 * @return A Maven artifact considered to be in-scope, but impacted by one that is not-in-scope; may be null.
	 */
	public Artifact getImpactedChildArtifact() {
		return this.impactedChildArtifact;
	}

}
