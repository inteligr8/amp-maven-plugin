/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.amp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.maven.plugin.logging.Log;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;

/**
 * This class provides the ability to de-select dependencies if they are
 * detected to be an Alfresco Module Package.  This includes dependencies of
 * the AMP or JAR package types.
 * 
 * @author brian@inteligr8.com
 */
public class AmpDependencyFilter implements DependencyFilter {

	private final Pattern modulePropertiesPattern = Pattern.compile("^alfresco/module/[^/]+/module\\.properties$");
	private final String moduleExtension = "amp";
	private final Set<String> ignoreDependenciesOfParentScopes = new HashSet<>(Arrays.asList("provided", "system"));
	
	private final int streamBufferSize = 16 * 1024;
	private final Log log;
	private final Charset charset;
	private final ArtifactResolutionCallback callback;
	private final String projectModuleId;
	
	private Set<String> moduleIds = new HashSet<String>();
	private Set<String> notModuleIds = new HashSet<String>();
	
	/**
	 * @param log The Maven plugin logger
	 * @param charsetName A charset for processing ZIP (JAR/AMP) files
	 * @param callback A Maven plugin callback
	 */
	public AmpDependencyFilter(Log log, String charsetName, ArtifactResolutionCallback callback) {
		this.log = log;
		this.charset = Charset.forName(charsetName);
		this.callback = callback;

		this.projectModuleId = this.getModuleId(this.callback.getProject().getArtifact());
	}
	
	private String getModuleId(org.apache.maven.artifact.Artifact artifact) {
		return artifact.getGroupId() + "." + artifact.getArtifactId();
	}
	
	private String getModuleId(Artifact artifact) {
		return artifact.getGroupId() + "." + artifact.getArtifactId();
	}
	
	@Override
	public boolean accept(DependencyNode node, List<DependencyNode> parents) {
		Artifact artifact = node.getArtifact();
		if (this.log.isDebugEnabled())
			this.log.debug("Checking dependency: " + artifact.getArtifactId());
		
		String possibleModuleId = this.getModuleId(artifact);
		if (this.projectModuleId.equals(possibleModuleId)) {
			// always include project itself, even if it is a module
			return true;
		}
		
		try {
			if (this.isOrIsInAlfrescoModule(possibleModuleId, node, parents.iterator())) {
				if (this.log.isDebugEnabled())
					this.log.debug("Not packaging library; detected as Alfresco Module or as dependency to other Alfresco Module: " + artifact.getArtifactId());
				return false;
			}
		} catch (NotInScopeException nise) {
			if (this.log.isDebugEnabled())
				this.log.debug("Not packaging library; detected as provided by another dependency [" + nise.getNotInScopeParentArtifact() + "]: " + artifact.getArtifactId());
			return false;
		}
		
		return true;
	}
	
	private boolean isOrIsInAlfrescoModule(String possibleModuleId, DependencyNode depNode, Iterator<DependencyNode> parents) throws NotInScopeException {
    	Artifact artifact = depNode.getArtifact();
    	
    	if (this.projectModuleId.equals(possibleModuleId)) {
    		return false;
    	} else if (depNode.getDependency() != null && this.ignoreDependenciesOfParentScopes.contains(depNode.getDependency().getScope())) {
			throw new NotInScopeException(artifact);
    	} else if (this.moduleIds.contains(possibleModuleId)) {
			if (this.log.isDebugEnabled())
				this.log.debug("Detected as Alfresco Module: " + artifact.getArtifactId());
			return true;
		} else if (this.notModuleIds.contains(possibleModuleId)) {
			return false;
		} else if (parents != null && parents.hasNext()) {
			DependencyNode parentNode = parents.next();
			String parentModuleId = this.getModuleId(parentNode.getArtifact());
			if (this.log.isDebugEnabled())
				this.log.debug("Checking parent dependency: " + artifact.getArtifactId());
			
			if (this.isOrIsInAlfrescoModule(parentModuleId, parentNode, parents)) {
				if (this.log.isDebugEnabled())
					this.log.debug("Detected as dependency to other Alfresco Module: " + artifact.getArtifactId());
				return true;
			}
		}
		
		// never seen this artifact or its parents
		return this.isAlfrescoModule(possibleModuleId, depNode);
	}
    
    private boolean isAlfrescoModule(String possibleModuleId, DependencyNode depNode) {
    	Artifact artifact = depNode.getArtifact();
    	
		if (this.moduleExtension.equalsIgnoreCase(artifact.getExtension())) {
			if (this.log.isDebugEnabled())
				this.log.debug("Detected as Alfresco Module: " + artifact.getArtifactId());
			this.moduleIds.add(possibleModuleId);
			return true;
		}
		
		File file = artifact.getFile();
		try {
			if (file == null) {
				if (this.log.isDebugEnabled())
					this.log.debug("Resolving dependency to get file: " + artifact.getArtifactId());
				ArtifactResult result = this.callback.resolveArtifact(new ArtifactRequest(depNode));
				if (result.isMissing() || !result.isResolved())
					throw new ArtifactResolutionException(Arrays.asList(result));
				artifact = result.getArtifact();
				file = artifact.getFile();
			}
			if (this.log.isDebugEnabled())
				this.log.debug("Checking dependency file: " + file);
			
			if (this.isAlfrescoModuleJar(file)) {
				this.moduleIds.add(possibleModuleId);
				if (this.log.isInfoEnabled())
					this.log.info("Detected as Alfresco Module: " + artifact.getArtifactId());
				return true;
			}
			
			this.notModuleIds.add(possibleModuleId);
			return false;
		} catch (ArtifactResolutionException are) {
			this.log.warn("An artifact could not be resolved; assuming it is not an Alfresco module and continuing");
			return false;
		} catch (IOException ie) {
			this.log.warn("An I/O issue occurred while inspecting a JAR to see if it is an Alfresco module; assuming it isn't and continuing");
			return false;
		}
    }
    
	/**
	 * @param file A Java I/O file instance.
	 * @return true if the file is a module; false otherwise.
	 * @throws IOException An I/O exception occurred.
	 */
    public boolean isAlfrescoModuleJar(File file) throws IOException {
    	FileInputStream fistream = new FileInputStream(file);
    	BufferedInputStream bistream = new BufferedInputStream(fistream, this.streamBufferSize);
    	ZipInputStream zstream = new ZipInputStream(bistream, this.charset);
    	try {
    		ZipEntry zentry = zstream.getNextEntry();
    		while (zentry != null) {
    			try {
	    			Matcher modPropsMatcher = this.modulePropertiesPattern.matcher(zentry.getName());
	    			if (modPropsMatcher.find())
	    				return true;
    			} finally {
    				zstream.closeEntry();
    			}
    			
    			zentry = zstream.getNextEntry();
    		}
    	} finally {
    		zstream.close();
    	}
    	
    	return false;
    }

}
