/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.amp;

import org.apache.maven.project.MavenProject;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;

/**
 * This interface provides a means for worker classes to call back into the
 * Maven Plugin classes for more information or features.
 * 
 * @author brian@inteligr8.com
 */
public interface ArtifactResolutionCallback {
	
	/**
	 * Retrieves the Maven plugin `MavenProject` object of the Maven project being executed.
	 * 
	 * @return A Maven project instance.
	 */
	MavenProject getProject();
	
	/**
	 * Attempts to resolve an artifact description with an existing/real artifact.
	 * 
	 * @param artifactRequest An artifact to query.
	 * @return An artifact found.
	 * @throws ArtifactResolutionException The artifact resolution failed.
	 */
	ArtifactResult resolveArtifact(ArtifactRequest artifactRequest) throws ArtifactResolutionException;

}
