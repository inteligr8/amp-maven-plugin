/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.amp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Exclusion;
import org.apache.maven.model.FileSet;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.DefaultDependencyResolutionRequest;
import org.apache.maven.project.DependencyResolutionException;
import org.apache.maven.project.DependencyResolutionRequest;
import org.apache.maven.project.DependencyResolutionResult;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectDependenciesResolver;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.util.FileUtils;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.impl.ArtifactResolver;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.util.filter.DependencyFilterUtils;
import org.eclipse.aether.util.filter.ExclusionsDependencyFilter;
import org.eclipse.aether.util.filter.ScopeDependencyFilter;

/**
 * This class implements the "amp" goal for this Maven plugin.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "amp", defaultPhase = LifecyclePhase.PACKAGE, threadSafe = true,
		requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class AmpMojo extends AbstractMojo implements ArtifactResolutionCallback {
	
	private final int streamBufferSize = 16384;
	
	@Parameter( defaultValue = "${project}", readonly = true )
	protected MavenProject project;
	
	@Parameter( defaultValue = "${session}", readonly = true )
	protected MavenSession session;
	
	@Requirement
	private ProjectDependenciesResolver resolver;
	
	@Requirement
	private ArtifactResolver artifactResolver;

	@Parameter( property = "classifier", required = false )
	protected String classifier;
	
	@Parameter( property = "outputFile", required = true, defaultValue = "${project.build.directory}/${project.artifactId}-${project.version}.amp" )
	protected String outputAmpFile;

	@Parameter( property = "moduleFile", required = true, defaultValue = "${project.build.directory}/${project.build.finalName}.${project.artifact.type}" )
	protected String moduleJarFile;
	
	@Parameter( property = "modulePropertyFile", required = false )
	protected String modulePropFile;
	
	@Parameter( property = "resources", required = false )
	protected List<Resource> resources;
	
	@Parameter( property = "dependencyIncludeScopes", required = true, defaultValue = "compile,runtime" )
	protected String includeScopes;
	
	@Parameter( property = "dependencyExclusions", required = false )
	protected List<Exclusion> excludeDependencies;
	
	@Parameter( property = "lib", required = false )
	protected List<FileSet> libDirectories;
	
	@Parameter( property = "charset", required = true, defaultValue = "utf-8" )
	protected String charsetName;
	
	@Parameter( property = "skip", required = true, defaultValue = "false" )
	protected boolean skip;
	
	@Override
	public MavenProject getProject() {
		return this.project;
	}

	@Override
    public void execute() throws MojoExecutionException {
    	if (this.skip) {
        	this.getLog().debug("Skipped AMP package");
    		return;
    	}
    	this.getLog().debug("Executing AMP packaging");
    	
    	this.classifier = StringUtils.trimToNull(this.classifier);
    	if (this.classifier != null) {
    		this.getLog().info("A classifier was specified; injecting classifier into module and output artifact names");
    		this.outputAmpFile = this.injectClassifier(this.outputAmpFile);
    	}

    	this.normalize();
    	
    	File ampFile = this.getOutputFile();
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("Writing AMP file: " + ampFile.getAbsolutePath());
    	
    	try {
	    	FileOutputStream fostream = new FileOutputStream(ampFile, false);
			BufferedOutputStream bostream = new BufferedOutputStream(fostream, this.streamBufferSize);
			ZipOutputStream zstream = new ZipOutputStream(bostream, Charset.forName(this.charsetName));
			try {
				this.zip(zstream);
			} finally {
				zstream.finish();
				zstream.close();
			}

    		if (this.getLog().isDebugEnabled())
    			this.getLog().debug("Wrote AMP file: " + ampFile.length());
    	} catch (IllegalStateException ise) {
    		this.getLog().warn(ise.getMessage());
    		ampFile.delete();
    		if (this.getLog().isDebugEnabled())
    			this.getLog().debug("Deleted AMP: " + ampFile.getAbsolutePath());
    	} catch (DependencyResolutionException dre) {
    		throw new MojoExecutionException("The dependencies could not be properly resolved", dre);
    	} catch (IOException ie) {
    		throw new MojoExecutionException("An I/O issue occurred", ie);
    	}
    }
    
    private String injectClassifier(String filename) {
    	int lastDot = filename.lastIndexOf('.');
    	if (lastDot < 0)
    		throw new IllegalArgumentException();
    	
    	int lastDash = filename.lastIndexOf('-', lastDot-1);
    	if (lastDash > 0) {
    		// see if the classifier was already specified
    		String possibleClassifier = filename.substring(lastDash+1, lastDot);
    		if (this.classifier.equals(possibleClassifier))
    			return filename;
    	}
    	
    	return filename.substring(0, lastDot) + '-' + this.classifier + filename.substring(lastDot);
    }
    
    private void normalize() {
    	this.outputAmpFile = StringUtils.trimToNull(this.outputAmpFile);
    	this.moduleJarFile = StringUtils.trimToNull(this.moduleJarFile);
    	this.modulePropFile = StringUtils.trimToNull(this.modulePropFile);
    	if (this.resources == null)
    		this.resources = Collections.emptyList();
    	this.includeScopes = this.includeScopes.trim();
    	if (this.libDirectories == null)
    		this.libDirectories = Collections.emptyList();
    	
    	this.getLog().debug("Normalized parameters");
    }
    
    private File getOutputFile() {
    	File ampFile = new File(this.outputAmpFile);
    	if (ampFile.exists())
    		ampFile.delete();
    	return ampFile;
    }
    
    private File getModuleFile() throws MojoExecutionException {
		if (this.classifier != null) {
        	String moduleJarFile = this.injectClassifier(this.moduleJarFile);
    		File jarFile = new File(moduleJarFile);
    		if (jarFile.exists())
				return jarFile;
		}

    	File jarFile = new File(this.moduleJarFile);
		if (jarFile.exists())
			return jarFile;

		throw new MojoExecutionException("The module JAR file does not exist: " + this.moduleJarFile);
    }
    
    private File getModulePropertyFile() throws MojoExecutionException {
    	File propFile = null;
    	if (this.modulePropFile != null) {
	    	propFile = new File(this.modulePropFile);
    	} else {
    		String outputDirectory = this.project.getBuild().getOutputDirectory();
    		File modulesDirectory = new File(outputDirectory, "alfresco/module");
    		if (!modulesDirectory.exists() || !modulesDirectory.isDirectory())
    			throw new MojoExecutionException("The module.properties file could not be found: 'alfresco/module' path does not exist");

    		File moduleDirectory = new File(modulesDirectory, this.project.getGroupId() + "." + this.project.getArtifactId());
    		if (!moduleDirectory.exists())
    			moduleDirectory = new File(modulesDirectory, this.project.getArtifactId());
    		if (!moduleDirectory.exists()) {
    			File[] files = modulesDirectory.listFiles();
    			if (files.length > 0) {
    				moduleDirectory = files[0];
	        		if (!moduleDirectory.exists())
	        			throw new MojoExecutionException("The module.properties file could not be found: 'alfresco/module' path does not exist");
    			}
    		}
    		
    		propFile = new File(moduleDirectory, "module.properties");
    	}
    	
    	if (!propFile.exists())
    		throw new MojoExecutionException("The module.properties file does not exist: " + propFile.getAbsolutePath());
    	return propFile;
    }
    
    private void zip(ZipOutputStream zstream) throws IOException, MojoExecutionException, DependencyResolutionException {
		for (Resource resource : this.resources)
			this.zipResource(zstream, resource);

    	File jarFile = this.getModuleFile();
		this.zipFile(zstream, jarFile, "lib");
		
		File propFile = this.getModulePropertyFile();
		this.zipFile(zstream, propFile, "");

		int libcount = 0;
		
		Collection<Dependency> deps = this.getDependencies();
		if (deps.isEmpty()) {
    		this.getLog().info("Zipped 0 dependencies");
		} else {
			for (Dependency dependency : deps) {
				File file = dependency.getArtifact().getFile();
				this.zipFile(zstream, file, "lib");
				libcount++;
			}

	    	if (this.getLog().isInfoEnabled())
	    		this.getLog().info("Zipped " + deps.size() + " dependencies");
		}
    	
    	for (FileSet fileset : this.libDirectories)
    		libcount += this.zipFileset(zstream, fileset, "lib");

		if (libcount == 0)
			this.getLog().warn("There are no external libraries to include; an AMP is not recommended");
    }
    
    private List<String> getExclusions() {
    	List<String> exclusions = new ArrayList<>(this.excludeDependencies.size());
    	for (Exclusion exclusion : this.excludeDependencies)
    		exclusions.add(exclusion.getGroupId() + ":" + exclusion.getArtifactId());
    	return exclusions;
    }
    
    private Collection<Dependency> getDependencies() throws DependencyResolutionException {
    	String[] includeScopes = StringUtils.split(this.includeScopes, ",");
    	
		ScopeDependencyFilter scopeFilter = new ScopeDependencyFilter(Arrays.asList(includeScopes), new ArrayList<String>(0));
		ExclusionsDependencyFilter exclusionFilter = new ExclusionsDependencyFilter(this.getExclusions());
		AmpDependencyFilter ampFilter = new AmpDependencyFilter(this.getLog(), this.charsetName, this);
		
		DependencyResolutionRequest request = new DefaultDependencyResolutionRequest(this.project, this.session.getRepositorySession());
		request.setResolutionFilter(DependencyFilterUtils.andFilter(scopeFilter, exclusionFilter, ampFilter));

		DependencyResolutionResult result = this.resolver.resolve(request);
		return result.getResolvedDependencies();
    }
    
    private void zipResource(ZipOutputStream zstream, Resource resource) throws MojoExecutionException, IOException {
    	String baseZipPath = resource.getTargetPath();
    	if (baseZipPath == null)
    		baseZipPath = "";
    	else if (!baseZipPath.endsWith("/"))
    		baseZipPath += "/";
    	this.zipFileset(zstream, resource, baseZipPath);
    }

    private int zipFileset(ZipOutputStream zstream, FileSet fileset, String targetPath) throws MojoExecutionException, IOException {
    	targetPath = StringUtils.trimToEmpty(targetPath);
    	if (targetPath.length() > 0 && !targetPath.endsWith("/"))
    		targetPath += "/";

    	File directory = new File(this.project.getBasedir(), fileset.getDirectory());
    	if (!directory.exists())
    		return 0;
    	if (!directory.isDirectory())
    		throw new MojoExecutionException("The fileset 'directory' must be a directory: " + fileset.getDirectory());

    	String includes = StringUtils.join(fileset.getIncludes(), ",");
    	String excludes = StringUtils.join(fileset.getExcludes(), ",");
    	List<String> filenames = FileUtils.getFileNames(directory, includes, excludes, false, true);
    	
    	if (this.getLog().isDebugEnabled())
    		this.getLog().debug("Zipping files in: " + fileset.getDirectory());
    	for (String filename : filenames) {
        	if (this.getLog().isDebugEnabled())
        		this.getLog().debug("Zipping file: " + filename);
        	
    		zstream.putNextEntry(new ZipEntry(filename));
    		
    		File file = new File(directory, filename);
    		FileInputStream fistream = new FileInputStream(file);
    		BufferedInputStream bistream = new BufferedInputStream(fistream, this.streamBufferSize);
    		try {
    			IOUtils.copy(bistream, zstream);
    		} finally {
    			bistream.close();
    		}
    		
    		zstream.closeEntry();
    	}

    	if (this.getLog().isInfoEnabled())
    		this.getLog().info("Zipped " + filenames.size() + " files: " + fileset.getDirectory());
    	
    	return filenames.size();
    }
    
    private void zipFile(ZipOutputStream zstream, File file, String targetPath) throws IOException {
    	targetPath = StringUtils.trimToEmpty(targetPath);
    	if (targetPath.length() > 0 && !targetPath.endsWith("/"))
    		targetPath += "/";
    	
    	if (this.getLog().isDebugEnabled())
    		this.getLog().debug("Zipping file: " + file.toString());
        
    	zstream.putNextEntry(new ZipEntry(targetPath + file.getName()));
    	
		FileInputStream fistream = new FileInputStream(file);
		BufferedInputStream bistream = new BufferedInputStream(fistream, this.streamBufferSize);
		try {
			IOUtils.copy(bistream, zstream);
		} finally {
			bistream.close();
		}
    	
    	zstream.closeEntry();
    }
    
    @Override
    public ArtifactResult resolveArtifact(ArtifactRequest artifactRequest) throws ArtifactResolutionException {
    	return this.artifactResolver.resolveArtifact(this.session.getRepositorySession(), artifactRequest);
    }
    
}
